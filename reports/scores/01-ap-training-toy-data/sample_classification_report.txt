                                                               precision    recall  f1-score   support

                                                       Others       0.99      0.99      0.99    271632
                    business?acquisitions-mergers?acquisition       0.92      0.93      0.92     10249
                business?acquisitions-mergers?acquisition-bid       0.91      0.93      0.92      2842
         business?acquisitions-mergers?acquisition-regulation       0.86      0.77      0.81       187
                         business?acquisitions-mergers?merger       0.95      0.98      0.96      1607
              business?acquisitions-mergers?merger-regulation       0.86      0.72      0.78        78
                          business?acquisitions-mergers?stake       0.92      0.93      0.92      4277
               business?acquisitions-mergers?unit-acquisition       0.88      0.86      0.87      4502
              business?analyst-ratings?analyst-ratings-change       0.99      0.99      0.99     13653
             business?analyst-ratings?analyst-ratings-history       0.97      0.94      0.95        77
                 business?analyst-ratings?analyst-ratings-set       0.99      1.00      1.00      3245
                                        business?assets?asset       0.97      0.96      0.96      3707
                             business?assets?commodity-assets       0.91      0.87      0.89       199
                             business?assets?company-for-sale       0.97      0.97      0.97       260
                                     business?assets?facility       0.96      0.94      0.95      5182
                          business?assets?headquarters-change       0.91      0.89      0.90       106
                                       business?assets?patent       0.99      0.99      0.99       550
                                business?assets?unit-for-sale       0.83      0.72      0.77       116
                               business?bankruptcy?bankruptcy       0.99      0.98      0.98       629
                          business?bankruptcy?bankruptcy-unit       0.77      0.89      0.83        27
                 business?credit-ratings?credit-rating-change       0.98      0.98      0.98      2993
                business?credit-ratings?credit-rating-outlook       0.97      0.99      0.98       374
                  business?credit-ratings?credit-rating-watch       0.96      0.97      0.96       167
                             business?credit?credit-extension       0.95      0.93      0.94       425
                                         business?credit?debt       0.95      0.96      0.95       728
                           business?credit?debt-restructuring       0.98      0.93      0.95        57
                                         business?credit?loan       0.84      0.87      0.85       156
                             business?credit?note-acquisition       0.89      0.89      0.89        76
                                    business?credit?note-sale       0.98      0.99      0.99      1651
                           business?credit?shelf-registration       0.99      0.99      0.99       116
                                  business?dividends?dividend       0.99      0.99      0.99     10443
                         business?dividends?dividend-guidance       0.91      0.92      0.92       453
                                   business?earnings?earnings       0.99      0.99      0.99     54672
                          business?earnings?earnings-estimate       0.96      0.95      0.95       781
                      business?earnings?earnings-expectations       1.00      0.99      0.99      9555
                          business?earnings?earnings-guidance       0.98      0.97      0.98      6874
             business?earnings?earnings-guidance-expectations       0.98      0.98      0.98       714
                         business?earnings?earnings-per-share       1.00      1.00      1.00     21520
                business?earnings?earnings-per-share-estimate       0.99      0.98      0.99      1504
            business?earnings?earnings-per-share-expectations       1.00      1.00      1.00     10150
                business?earnings?earnings-per-share-guidance       0.98      0.99      0.99      3262
   business?earnings?earnings-per-share-guidance-expectations       0.99      0.99      0.99      1467
                          business?earnings?earnings-revision       0.99      0.93      0.96       114
                                       business?earnings?ebit       1.00      1.00      1.00      4752
                          business?earnings?ebit-expectations       1.00      1.00      1.00      4433
                              business?earnings?ebit-guidance       1.00      1.00      1.00       946
                 business?earnings?ebit-guidance-expectations       1.00      1.00      1.00       507
                                      business?earnings?ebita       1.00      0.99      1.00       149
                         business?earnings?ebita-expectations       1.00      1.00      1.00       136
                                     business?earnings?ebitda       1.00      1.00      1.00      4471
                        business?earnings?ebitda-expectations       1.00      1.00      1.00      3495
                            business?earnings?ebitda-guidance       0.97      0.98      0.98       277
               business?earnings?ebitda-guidance-expectations       1.00      1.00      1.00       209
                            business?earnings?interest-income       1.00      1.00      1.00       636
                         business?earnings?operating-earnings       0.94      0.98      0.96      1999
                business?earnings?operating-earnings-guidance       0.88      0.91      0.90       143
                            business?earnings?pretax-earnings       0.99      1.00      0.99      5633
               business?earnings?pretax-earnings-expectations       1.00      1.00      1.00      4725
                   business?earnings?pretax-earnings-guidance       0.99      1.00      0.99      1294
      business?earnings?pretax-earnings-guidance-expectations       0.99      1.00      1.00       384
                             business?equity-actions?buybacks       0.98      0.99      0.99      2144
                                business?equity-actions?capex       0.99      0.99      0.99      1818
                       business?equity-actions?capex-guidance       0.97      0.97      0.97       434
                     business?equity-actions?capital-increase       0.98      0.99      0.98       447
                             business?equity-actions?expenses       0.97      0.97      0.97      2543
                    business?equity-actions?expenses-guidance       0.97      0.95      0.96       614
                          business?equity-actions?fundraising       0.99      0.99      0.99      1104
                        business?equity-actions?going-private       0.97      1.00      0.98        65
              business?equity-actions?initial-public-offering       0.96      0.99      0.97       862
        business?equity-actions?initial-public-offering-price       0.98      0.96      0.97       271
         business?equity-actions?initial-public-offering-unit       1.00      0.55      0.71        31
                           business?equity-actions?investment       0.96      0.94      0.95      2151
                          business?equity-actions?name-change       0.99      0.97      0.98       206
                            business?equity-actions?ownership       0.98      0.97      0.98      6876
                    business?equity-actions?private-placement       0.99      1.00      0.99       457
                      business?equity-actions?public-offering       0.99      0.98      0.98      1286
                       business?equity-actions?reorganization       0.96      0.97      0.96      1040
                  business?equity-actions?reorganization-unit       0.94      0.83      0.88       107
                 business?equity-actions?reverse-stock-splits       0.94      0.97      0.96        79
                         business?equity-actions?rights-issue       0.99      0.99      0.99       308
                              business?equity-actions?savings       1.00      0.99      0.99        88
                     business?equity-actions?savings-guidance       0.98      0.93      0.95        44
              business?equity-actions?shareholder-rights-plan       1.00      1.00      1.00        59
                   business?equity-actions?shelf-registration       1.00      0.97      0.99        69
                             business?equity-actions?spin-off       0.99      0.98      0.99       528
                         business?equity-actions?stock-splits       0.99      0.94      0.96        79
                              business?equity-actions?trading       0.99      0.99      0.99      1607
                                business?exploration?drilling       0.98      0.99      0.98       887
                      business?exploration?resource-discovery       0.97      0.92      0.95       166
                               business?indexes?index-listing       0.98      0.97      0.98       170
              business?industrial-accidents?aircraft-accident       0.95      0.87      0.91        69
              business?industrial-accidents?facility-accident       0.80      0.88      0.84        65
                  business?industrial-accidents?force-majeure       1.00      1.00      1.00        30
      business?industrial-accidents?public-transport-accident       0.94      0.83      0.88        18
                         business?insider-trading?insider-buy       0.99      0.99      0.99      4016
                        business?insider-trading?insider-gift       1.00      1.00      1.00       356
                        business?insider-trading?insider-sell       0.99      0.99      0.99      5008
                   business?insider-trading?insider-surrender       1.00      1.00      1.00      1898
                   business?insider-trading?sell-registration       1.00      1.00      1.00      3683
                    business?investor-relations?board-meeting       0.97      0.92      0.94       119
                  business?investor-relations?conference-call       1.00      1.00      1.00      8767
    business?investor-relations?major-shareholders-disclosure       1.00      1.00      1.00      2051
             business?investor-relations?shareholders-meeting       0.99      0.99      0.99       968
               business?labor-issues?board-member-appointment       0.88      0.89      0.89      1640
               business?labor-issues?board-member-resignation       0.88      0.86      0.87       583
                business?labor-issues?board-member-retirement       0.67      0.64      0.65        85
                  business?labor-issues?executive-appointment       0.96      0.96      0.96      7974
                 business?labor-issues?executive-compensation       0.97      0.99      0.98       170
                        business?labor-issues?executive-death       0.98      0.96      0.97       101
                       business?labor-issues?executive-firing       0.94      0.89      0.91       229
                       business?labor-issues?executive-health       0.88      0.84      0.86        25
                   business?labor-issues?executive-incentives       0.96      0.66      0.78        38
                  business?labor-issues?executive-resignation       0.94      0.94      0.94      3321
                   business?labor-issues?executive-retirement       0.76      0.77      0.77       417
                       business?labor-issues?executive-salary       0.97      0.96      0.97       364
                      business?labor-issues?executive-scandal       0.95      0.82      0.88        51
                       business?labor-issues?executive-search       0.88      0.88      0.88        24
                                business?labor-issues?hirings       0.96      0.94      0.95       668
                                business?labor-issues?layoffs       0.99      0.98      0.98      2137
                             business?labor-issues?union-pact       0.95      0.73      0.82        48
                         business?labor-issues?workers-strike       0.94      0.98      0.96       246
                       business?labor-issues?workforce-salary       0.84      0.79      0.81        47
                               business?marketing?campaign-ad       0.97      0.94      0.96       377
                                business?marketing?conference       0.99      0.98      0.99      1950
                          business?marketing?press-conference       0.93      0.96      0.94        67
                            business?order-imbalances?buy-moc       1.00      1.00      1.00      2166
                            business?order-imbalances?buy-moo       1.00      1.00      1.00       148
                    business?order-imbalances?delay-imbalance       1.00      1.00      1.00        36
                           business?order-imbalances?sell-moc       1.00      1.00      1.00      1823
                           business?order-imbalances?sell-moo       1.00      1.00      1.00       176
                          business?partnerships?joint-venture       0.95      0.95      0.95       848
                            business?partnerships?partnership       0.98      0.97      0.97      6670
                          business?price-targets?price-target       1.00      1.00      1.00      5330
                             business?products-services?award       0.97      0.97      0.97      1419
              business?products-services?business-combination       0.96      0.96      0.96       135
                 business?products-services?business-contract       0.95      0.94      0.95     10803
                   business?products-services?clinical-trials       0.98      0.97      0.98      1557
business?products-services?clinical-trials-patient-enrollment       0.94      0.99      0.96       135
                       business?products-services?competition       1.00      0.98      0.99       109
                            business?products-services?demand       0.90      0.81      0.85       332
                   business?products-services?demand-guidance       0.90      0.82      0.86       102
            business?products-services?fast-track-designation       1.00      1.00      1.00        26
               business?products-services?government-contract       0.81      0.78      0.80       664
                             business?products-services?grant       0.89      0.78      0.83        40
                      business?products-services?market-entry       0.91      0.95      0.93       608
                       business?products-services?market-exit       0.93      0.88      0.91       131
                   business?products-services?market-guidance       0.96      0.88      0.92        56
                      business?products-services?market-share       0.97      0.97      0.97       613
           business?products-services?orphan-drug-designation       0.98      1.00      0.99        63
               business?products-services?product-development       0.96      0.96      0.96      1854
              business?products-services?product-discontinued       0.78      0.88      0.83       402
               business?products-services?product-enhancement       0.94      0.93      0.94      2407
                    business?products-services?product-outage       0.93      0.83      0.88       200
                   business?products-services?product-pricing       0.95      0.96      0.96      1253
                 business?products-services?product-promotion       0.96      0.83      0.89        29
                    business?products-services?product-recall       0.98      0.97      0.98       732
                   business?products-services?product-release       0.97      0.97      0.97     11011
                   business?products-services?product-resumed       0.90      0.92      0.91        84
                    business?products-services?product-review       0.97      0.95      0.96       847
                 business?products-services?project-abandoned       0.75      0.52      0.62        23
    business?products-services?regulatory-product-application       0.88      0.84      0.86        77
       business?products-services?regulatory-product-approval       0.92      0.94      0.93       527
                            business?products-services?supply       0.95      0.96      0.96      1597
                   business?products-services?supply-guidance       0.92      0.90      0.91       532
                      business?regulatory?auditor-appointment       1.00      0.89      0.94        44
                   business?regulatory?exchange-noncompliance       0.96      1.00      0.98        27
                 business?regulatory?regulatory-investigation       0.91      0.90      0.91       529
                   business?regulatory?regulatory-stress-test       1.00      0.97      0.99        35
                           business?revenues?operating-margin       0.97      0.98      0.98       221
                  business?revenues?operating-margin-guidance       0.97      0.94      0.96        36
                                    business?revenues?revenue       0.98      0.98      0.98     37351
                           business?revenues?revenue-estimate       0.83      0.89      0.86       135
                           business?revenues?revenue-guidance       0.97      0.96      0.96      6992
                             business?revenues?revenue-volume       0.83      0.87      0.85      2255
                           business?revenues?same-store-sales       0.92      0.95      0.93      1126
                  business?revenues?same-store-sales-guidance       0.86      0.84      0.85       171
                              business?stock-picks?stock-pick       0.91      0.94      0.92       403
                            business?stock-prices?stock-price       1.00      1.00      1.00     67908
          business?technical-analysis?relative-strength-index       1.00      1.00      1.00       902
            business?technical-analysis?technical-price-level       1.00      1.00      1.00      2162
                   business?technical-analysis?technical-view       1.00      1.00      1.00      7663
                                 society?civil-unrest?protest       0.90      0.88      0.89        43
                    society?corporate-responsibility?donation       0.99      0.95      0.97       278
                 society?corporate-responsibility?sponsorship       0.94      0.64      0.76        25
                                        society?crime?robbery       1.00      0.88      0.94        26
                                       society?crime?shooting       0.94      0.85      0.89        53
                        society?legal?antitrust-investigation       0.68      0.71      0.70        21
                                 society?legal?antitrust-suit       0.81      0.85      0.83        34
                                 society?legal?discrimination       1.00      0.92      0.96        26
                                          society?legal?fraud       0.89      0.91      0.90        64
                                   society?legal?legal-issues       0.97      0.96      0.97      2398
                            society?legal?patent-infringement       0.95      0.94      0.94       155
                                      society?legal?sanctions       0.97      0.92      0.94       792
                                     society?legal?settlement       0.93      0.96      0.95      1015
                                        society?legal?verdict       0.94      0.92      0.93       412
                               society?security?cyber-attacks       0.98      0.97      0.98       188
                                   society?security?explosion       0.92      0.81      0.86        43
                     society?transportation?emergency-landing       0.97      1.00      0.98        31
             society?transportation?transportation-disruption       0.91      0.78      0.84       165
                                 society?war-conflict?bombing       0.90      0.86      0.88        22
                                 society?war-conflict?embargo       0.93      0.74      0.82        34

                                                     accuracy                           0.98    749066
                                                    macro avg       0.95      0.93      0.94    749066
                                                 weighted avg       0.98      0.98      0.98    749066
