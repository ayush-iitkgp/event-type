                          precision    recall  f1-score   support

     Business Continuity       0.70      0.31      0.43      5938
       Criminal Activity       0.65      0.97      0.78     14272
    Industrial Espionage       0.00      0.00      0.00        16
                   Other       0.71      0.10      0.17      2243
                  Others       0.99      1.00      1.00     14857
Terrorism and Insurgency       0.73      0.19      0.31      2314

                accuracy                           0.79     39640
               macro avg       0.63      0.43      0.45     39640
            weighted avg       0.80      0.79      0.75     39640
