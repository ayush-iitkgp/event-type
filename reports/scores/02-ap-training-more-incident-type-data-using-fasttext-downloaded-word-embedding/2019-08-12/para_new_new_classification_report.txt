                          precision    recall  f1-score   support

     Business Continuity       0.00      0.00      0.00      6420
       Criminal Activity       0.00      0.00      0.00     14858
    Industrial Espionage       0.00      0.00      0.00        16
                   Other       0.00      0.00      0.00      2498
                  Others       0.69      1.00      0.82     59431
Terrorism and Insurgency       0.00      0.00      0.00      2637

                accuracy                           0.69     85860
               macro avg       0.12      0.17      0.14     85860
            weighted avg       0.48      0.69      0.57     85860
