                                                                   precision    recall  f1-score   support

                   Business Continuity?Adverse Government Actions       0.00      0.00      0.00        26
                       Business Continuity?Adverse NGO Activities       0.00      0.00      0.00         3
                                        Business Continuity?Arson       0.00      0.00      0.00         1
                                Business Continuity?Cargo Threats       0.00      0.00      0.00         1
                                  Business Continuity?Cybercrimes       0.00      0.00      0.00         2
                                       Business Continuity?Health       0.00      0.00      0.00         2
                                Business Continuity?Labor Actions       0.00      0.00      0.00        12
                             Business Continuity?Natural Disaster       0.00      0.00      0.00         2
                             Business Continuity?Political Unrest       0.57      1.00      0.73        65
                                  Criminal Activity?Armed Assault       0.00      0.00      0.00        11
                                          Criminal Activity?Arson       0.00      0.00      0.00         3
       Criminal Activity?Assassination or Attempted Assassination       0.00      0.00      0.00         2
               Criminal Activity?Attacks against First Responders       0.00      0.00      0.00         4
                                        Criminal Activity?Bombing       0.00      0.00      0.00         6
                                  Criminal Activity?Cargo Threats       0.00      0.00      0.00         3
                                     Criminal Activity?Corruption       0.00      0.00      0.00         5
           Criminal Activity?Crimes Targeting Expats and Tourists       0.00      0.00      0.00         9
                Criminal Activity?Crimes Targeting Public Figures       0.00      0.00      0.00         9
                                    Criminal Activity?Cybercrimes       0.00      0.00      0.00        13
                                 Criminal Activity?Cyberespionage       0.00      0.00      0.00         2
                               Criminal Activity?Drug Trafficking       0.00      0.00      0.00         6
                           Criminal Activity?Executive Protection       0.00      0.00      0.00         3
                  Criminal Activity?Kidnapping and Hostage Taking       0.00      0.00      0.00        16
                                Criminal Activity?Organized Crime       0.16      1.00      0.27        22
                        Criminal Activity?Other Criminal Activity       0.00      0.00      0.00        11
                       Criminal Activity?Plot or Attempted Attack       0.00      0.00      0.00         1
                               Criminal Activity?Political Unrest       0.00      0.00      0.00         3
                           Criminal Activity?Seizure or Hijacking       0.00      0.00      0.00         3
                                          Criminal Activity?Theft       0.00      0.00      0.00         2
                              Criminal Activity?Vehicular Assault       0.00      0.00      0.00         1
           Criminal Activity?Workplace Violence or Insider Threat       0.00      0.00      0.00         5
                              Industrial Espionage?Cyberespionage       0.67      1.00      0.80         6
                       Industrial Espionage?Traditional Espionage       0.00      0.00      0.00         3
              Terrorism and Insurgency?Adverse Government Actions       0.00      0.00      0.00         1
                           Terrorism and Insurgency?Armed Assault       0.00      0.00      0.00        23
                                   Terrorism and Insurgency?Arson       0.00      0.00      0.00         3
Terrorism and Insurgency?Assassination or Attempted Assassination       0.00      0.00      0.00         1
        Terrorism and Insurgency?Attacks against First Responders       0.00      0.00      0.00         5
                                 Terrorism and Insurgency?Bombing       0.33      1.00      0.50        42
                           Terrorism and Insurgency?Cargo Threats       0.00      0.00      0.00         1
    Terrorism and Insurgency?Crimes Targeting Expats and Tourists       0.00      0.00      0.00         6
         Terrorism and Insurgency?Crimes Targeting Public Figures       0.00      0.00      0.00         2
           Terrorism and Insurgency?Kidnapping and Hostage Taking       0.00      0.00      0.00        13
                         Terrorism and Insurgency?Organized Crime       0.00      0.00      0.00         3
                 Terrorism and Insurgency?Other Criminal Activity       0.00      0.00      0.00         4
                Terrorism and Insurgency?Plot or Attempted Attack       0.00      0.00      0.00         8
                        Terrorism and Insurgency?Political Unrest       0.00      0.00      0.00         7
                    Terrorism and Insurgency?Seizure or Hijacking       0.00      0.00      0.00         1
                       Terrorism and Insurgency?Vehicular Assault       0.00      0.00      0.00         7

                                                         accuracy                           0.35       390
                                                        macro avg       0.04      0.08      0.05       390
                                                     weighted avg       0.15      0.35      0.20       390
