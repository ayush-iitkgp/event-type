from setuptools import find_packages, setup

setup(
    name='eventtypes',
    packages=["src"],
    version='0.1.0',
    description='This repository contains all the analysis, ipython notebooks and python files for detecting an event-type that a news article describes',
    author='Event Registry',
    license='MIT',
)
