import os
pwd = 'D:\\Users\\xlikedemo\\work\\event-type'
os. chdir(pwd)

import pandas as pd
import sys
import zipfile

year = 2014

zip_dir_name = "D:\\Users\\xlikedemo\\work\\Ravenpack\RavenPackAnalytics_Companies_1.0_" + str(year) + ".zip"

zip_file = zipfile.ZipFile(zip_dir_name)

df = pd.concat([pd.read_csv(zip_file.open(text_file.filename), usecols=['EVENT_TEXT', 'SOURCE_NAME', 'TOPIC', 'GROUP', 'TYPE', 'TIMESTAMP_UTC']) 
                for text_file in zip_file.infolist() 
               if text_file.filename.endswith('.csv')], ignore_index=True)
print ("Colum names are:")
print (df.columns)

len(df)

non_empty_columns = ['EVENT_TEXT', 'SOURCE_NAME', 'TOPIC', 'GROUP', 'TYPE']
df.dropna(inplace=True, subset=non_empty_columns)

len(df)

non_empty_columns = ['EVENT_TEXT', 'SOURCE_NAME', 'TOPIC', 'GROUP', 'TYPE']
df.drop_duplicates(inplace=True, subset=non_empty_columns)

df['TOPICGROUPTYPE'] = df['TOPIC'] + '?' + df['GROUP'] + '?' + df['TOPIC']

df['TIMESTAMP_UTC'] = pd.to_datetime(df['TIMESTAMP_UTC'])

df.head()

df_uri = pd.read_csv("data/external/ravenpack/2014_all_sources_and_source_uri.csv")

df_uri[df_uri["Source Name"] == "Benzinga"]
# benzinga.com

frequent_source_names  = df['SOURCE_NAME'].value_counts().index.tolist()

least_source_names = frequent_source_names[::-1]

least_source_names_1000 = least_source_names[3435:3476]

print(least_source_names_1000)

from dateutil.parser import parse
import calendar

from multiprocessing import Process, Pool
import glob
import json
from nltk import sent_tokenize

df[df.SOURCE_NAME == "Yahoo! Finance"]['TOPICGROUPTYPE'].nunique()

least_source_names_1000[0:10]

from multiprocessing import Process, current_process

def find_match_topicgrouptypewise(df_er_all_articles, source_uri, month, event_type):
    new_filename = "data/external/ravenpack/" + str(year) + '/' + str(source_uri) + '/' + 'rp' + str(month) + str(event_type.replace('?', '_')) + '.csv'
    if os.path.exists(new_filename):
        df_tmp = pd.read_csv(new_filename)
        all_lines = df_tmp['EVENT_TEXT'].astype(str).values
        df_match = pd.DataFrame(columns = ["TOPICGROUPTYPE", "RavenPack Text", "ER Text", "Article URI", 
                                       "Source Name", "Source URI", "Index", "Event URI"])
        for line in all_lines:
            line = line.replace("|", "")
            try:
                cond_3 = df_er_all_articles['ER Text'].str.lower().str.contains(line.lower())
            except:
                continue
            df_line_match = df_er_all_articles[cond_3]
            if len(df_line_match) == 0:
                continue
            lines_list = [line] * len(df_line_match)
            df_line_match.columns = ["ER Text", "Article URI", "Source URI", "Index", "Event URI"]
            df_line_match["RavenPack Text"] = lines_list
            df_line_match["Source Name"] = [source_name] * len(df_line_match)
            df_line_match['TOPICGROUPTYPE'] =  [event_type] * len(df_line_match)
            df_line_match = df_line_match[['RavenPack Text','ER Text', 'Index', 'TOPICGROUPTYPE', 'Article URI', 'Event URI',  'Source Name',
                                       'Source URI']]
            df_match = pd.concat([df_match, df_line_match], ignore_index = True)
        if len(df_match) != 0:
            data_dir = 'data/external/ravenpack/' + str(year) + '/' + str(source_uri) + '/'
            event_type = event_type.replace('?', '_')
            filename = data_dir + "10_23_match_index_event_uri_" + str(month) + '_' + event_type + ".csv"
            if os.path.isfile(filename):
                filename_2 = data_dir + "10_23_2_match_index_event_uri_" + str(month) +  '_' + event_type + ".csv"
                df_match.to_csv(filename_2, index = False, sep = "\t")
            else:
                df_match.to_csv(filename, index = False, sep = "\t")
            print("Match found for the [%s] for the month %d is %d" %(source_uri, month, len(df_match)))


def find_match_monthwise(source_uri, month):
    proc_name = current_process().name
    print("[%s] started" %proc_name)
    new_filename = "data/external/ravenpack/" + str(year) +'/' + str(source_uri) + '/' + 'er' + str(month) + '.csv'
    print(new_filename)
    if os.path.exists(new_filename):
        df_er_all_articles = pd.read_csv(new_filename)
        if len(df_er_all_articles) == 0:
            return
        
        new_filename = "data/external/ravenpack/" + str(year) + '/' + str(source_uri) + '/' + 'rp' + str(month) + '.csv'

        df_tmp = pd.read_csv(new_filename)
        topic_group_types = df_tmp['TOPICGROUPTYPE'].unique()
        for event_type in topic_group_types:
            find_match_topicgrouptypewise(df_er_all_articles, source_uri, month, event_type)    


import time
total_match_found = 0
start_time = time.time()
i = 0
for source_name in least_source_names_1000[0:10]:
    i = i + 1
    if i % 10 == 0:
        print("Match completed for %d sources" %i)
    
    source_uri = df_uri[df_uri["Source Name"] == source_name]["Source URI"].values[0]
    if pd.isna(source_uri):
        continue
    print("Match started for %s" %source_uri)
    procs = []
    months = range(1, 13, 1)
    for month in months:
        proc = Process(target=find_match_monthwise, args=(source_uri, month))
        procs.append(proc)
        proc.start()
    for proc in procs:
        proc.join()
    
    print("Match completed for %s" %source_uri)
        
        
end_time = time.time()    
print("Total time taken for matching is %s hours" %str((end_time-start_time)/3600.0))