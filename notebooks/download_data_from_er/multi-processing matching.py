import os
import pandas as pd
import sys
import zipfile
from multiprocessing import Process, current_process
import time
from dateutil.parser import parse
import calendar
import glob
import json
from nltk import sent_tokenize
from helpers import *

def extract_news_body_and_index_event_uri_from_json_monthwise(source_uri, month):
    data_dir = 'data/external/ravenpack/' + str(year) + '/' + str(source_uri) + '/'
    files = glob.glob(data_dir + '*_er_data.json')
    new_filename = "data/external/ravenpack/" + str(year) +'/' + str(source_uri) + '/' + 'er' + str(month) + '.csv'
    df = pd.DataFrame()
    all_lines = []
    uris = []
    source_uris = []
    all_indices = []
    event_uris = []
    for file in files:        
        with open(file, 'r') as f:
            json_list = json.load(f)
            for json_ in json_list:
                j = 0
                er_month = parse(json_['dateTime']).month
                if er_month <= month and er_month > month - 1:
                    news_body = json_["body"]
                    uri = json_["uri"]
                    event_uri = json_["eventUri"]
                    s_uri = json_["source"]["uri"]
                    all_lines.append(json_["title"])
                    uris.append(uri)
                    source_uris.append(s_uri)
                    all_indices.append(j)
                    event_uris.append(event_uri)
                    lines = sent_tokenize(news_body)
                    for line in lines:
                        j = j + 1
                        all_lines.append(line)
                        uris.append(uri)
                        source_uris.append(s_uri)
                        all_indices.append(j)
                        event_uris.append(event_uri)

    df['ER Text'] = all_lines
    df['ARTICLE_URI'] = uris
    df['SOURCE_URI'] = source_uris
    df['INDEX'] = all_indices
    df["EVENT_URI"] = event_uris
    
    df.to_csv(new_filename, index = False)
    return df

def generate_rp_data_monthwise(df_all_articles, source_uri, month):
    if pd.isna(source_uri):
        return
    
    if len(df_all_articles) == 0:
        return
    
    cond_1 = df.SOURCE_NAME == source_name
    first_day = str(year) + '-' + str(month) + '-01'
    last_day = str(year) + '-' + str(month) + '-' + str(calendar.monthrange(year , month)[1])
    cond_2 = (df['TIMESTAMP_UTC'] >= first_day) & (df['TIMESTAMP_UTC'] <= last_day)
    df_tmp = df[cond_1 & cond_2]

    topic_group_types = df_tmp['TOPICGROUPTYPE'].unique()
    
    new_filename = "data/external/ravenpack/" + str(year) + '/' + str(source_uri) + '/' + 'rp' + str(month) + '.csv'
    
    df_tmp.to_csv(new_filename, index = False)
    for event_type in topic_group_types:
        generate_rp_data_topicgrouptypewise(df_tmp, df_all_articles, source_uri, month, event_type)    

def generate_rp_data_topicgrouptypewise(df_tmp, df_all_articles, source_uri, month, event_type):
    new_filename = "data/external/ravenpack/" + str(year) + '/' + str(source_uri) + '/' + 'rp' + str(month) + str(event_type.replace('?', '_')) + '.csv'
    cond_3 = df_tmp.TOPICGROUPTYPE == event_type
    df_tmp_tmp = df_tmp[cond_3]
    
    df_tmp_tmp.to_csv(new_filename, index = False)


def find_match_topicgrouptypewise(df_er_all_articles, source_uri, month, event_type):
    new_filename = "data/external/ravenpack/" + str(year) + '/' + str(source_uri) + '/' + 'rp' + str(month) + str(event_type.replace('?', '_')) + '.csv'
    if os.path.exists(new_filename):
        df_tmp = pd.read_csv(new_filename)
        if len(df_tmp) == 0:
            return
        all_lines = df_tmp['EVENT_TEXT'].astype(str).values
        df_match = pd.DataFrame(columns = ["TOPICGROUPTYPE", "RavenPack Text", "ER Text", "Article URI", 
                                       "Source Name", "Source URI", "Index", "Event URI"])
        for line in all_lines:
            line = line.replace("|", "")
            try:
                cond_3 = df_er_all_articles['ER Text'].str.lower().str.contains(line.lower())
            except:
                continue
            df_line_match = df_er_all_articles[cond_3]
            if len(df_line_match) == 0:
                continue
            lines_list = [line] * len(df_line_match)
            df_line_match.columns = ["ER Text", "Article URI", "Source URI", "Index", "Event URI"]
            df_line_match["RavenPack Text"] = lines_list
            df_line_match["Source Name"] = [source_name] * len(df_line_match)
            df_line_match['TOPICGROUPTYPE'] =  [event_type] * len(df_line_match)
            df_line_match = df_line_match[['RavenPack Text','ER Text', 'Index', 'TOPICGROUPTYPE', 'Article URI', 'Event URI',  'Source Name',
                                       'Source URI']]
            df_match = pd.concat([df_match, df_line_match], ignore_index = True)
        if len(df_match) != 0:
            data_dir = 'data/external/ravenpack/' + str(year) + '/' + str(source_uri) + '/'
            event_type = event_type.replace('?', '_')
            filename = data_dir + "10_23_match_index_event_uri_" + str(month) + '_' + event_type + ".csv"
            if os.path.isfile(filename):
                filename_2 = data_dir + "10_23_2_match_index_event_uri_" + str(month) +  '_' + event_type + ".csv"
                df_match.to_csv(filename_2, index = False, sep = "\t")
            else:
                df_match.to_csv(filename, index = False, sep = "\t")
            print("Match found for the [%s] for the month %d is %d" %(source_uri, month, len(df_match)))


def find_match_monthwise(source_uri, month):
    proc_name = current_process().name
    print("[%s] started" %proc_name)
    new_filename = "data/external/ravenpack/" + str(year) +'/' + str(source_uri) + '/' + 'er' + str(month) + '.csv'
    print(new_filename)
    if os.path.exists(new_filename):
        df_er_all_articles = pd.read_csv(new_filename)
        if len(df_er_all_articles) == 0:
            return
        
        new_filename = "data/external/ravenpack/" + str(year) + '/' + str(source_uri) + '/' + 'rp' + str(month) + '.csv'

        df_tmp = pd.read_csv(new_filename)
        topic_group_types = df_tmp['TOPICGROUPTYPE'].unique()
        for event_type in topic_group_types:
            find_match_topicgrouptypewise(df_er_all_articles, source_uri, month, event_type)    


pwd = 'D:\\Users\\xlikedemo\\work\\event-type'
os.chdir(pwd)

year = 2014

zip_dir_name = "D:\\Users\\xlikedemo\\work\\Ravenpack\RavenPackAnalytics_Companies_1.0_" + str(year) + ".zip"

zip_file = zipfile.ZipFile(zip_dir_name)

df = pd.concat([pd.read_csv(zip_file.open(text_file.filename), 
	usecols=['EVENT_TEXT', 'SOURCE_NAME', 'TOPIC', 'GROUP', 'TYPE', 'TIMESTAMP_UTC']) 
                for text_file in zip_file.infolist() 
               if text_file.filename.endswith('.csv')], ignore_index=True)
print ("Colum names are:")
print (df.columns)

print(len(df))

non_empty_columns = ['EVENT_TEXT', 'SOURCE_NAME', 'TOPIC', 'GROUP', 'TYPE']
df.dropna(inplace=True, subset=non_empty_columns)

print(len(df))

non_empty_columns = ['EVENT_TEXT', 'SOURCE_NAME', 'TOPIC', 'GROUP', 'TYPE']
df.drop_duplicates(inplace=True, subset=non_empty_columns)

df['TOPICGROUPTYPE'] = df['TOPIC'] + '?' + df['GROUP'] + '?' + df['TOPIC']

df['TIMESTAMP_UTC'] = pd.to_datetime(df['TIMESTAMP_UTC'])

print(df.head())

df_uri = pd.read_csv("data/external/ravenpack/2014_all_sources_and_source_uri.csv")

print(df_uri[df_uri["Source Name"] == "Benzinga"])
# benzinga.com

frequent_source_names  = df['SOURCE_NAME'].value_counts().index.tolist()

least_source_names = frequent_source_names[::-1]

least_source_names_1000 = least_source_names[3435:3437]

print(least_source_names_1000)


# print("Data generation started")
# i = 0
# for source_name in least_source_names_1000:
#     i = i + 1
#     if i % 10 == 0:
#         print("Match completed for %d sources." %i) 
#     source_uri = df_uri[df_uri["Source Name"] == source_name]["Source URI"].values[0]
#     if pd.isna(source_uri):
#             continue
#     months = range(1, 13, 1)
#     for month in months:
#         df_all_er_articles = extract_news_body_and_index_event_uri_from_json_monthwise(source_uri, month)
#         generate_rp_data_monthwise(df_all_er_articles, source_uri, month)
#     print("Completed for source %s" %source_uri)


if __name__ == '__main__':
	print("Data matching started")
	total_match_found = 0
	start_time = time.time()
	i = 0
	for source_name in least_source_names_1000:
		i = i + 1
		if i % 10 == 0:
			print("Match completed for %d sources" %i)

		source_uri = df_uri[df_uri["Source Name"] == source_name]["Source URI"].values[0]
		if pd.isna(source_uri):
			continue
		print("Match started for %s" %source_uri)
		procs = []
		months = range(1, 13, 1)
		for month in months:
			proc = Process(target=find_match_monthwise, args=(source_uri, month))
			procs.append(proc)
			proc.start()
		for proc in procs:
			proc.join()
		print("Match completed for %s" %source_uri)
        
        
end_time = time.time()    
print("Total time taken for matching is %s hours" %str((end_time-start_time)/3600.0))