from stanfordcorenlp import StanfordCoreNLP
import pandas as pd
from multiprocessing import Process, current_process
import glob
import os

# Important columns in the data
msg_column = 'EVENT_TEXT'
msg_clean_column = 'EVENT_TEXT_CLEANED'
msg_column_ner = 'EVENT_TEXT_NER'
topic_column = 'TOPIC'
group_column = 'GROUP'
type_column = 'TYPE'
topic_group_column = topic_column + group_column
topic_group_type_column = topic_column + group_column + type_column
nlp = StanfordCoreNLP('http://localhost', port = 9001)

def smaller_files():
    df_train_others = pd.read_csv("data/processed/external/others_train.csv")
    range_ = range(0, len(df_train_others), 2000000)
    for i in range(0, len(range_)):
        print (i+1)
        if i == len(range_) - 1 :
            temp_df = df_train_others[range_[i]:len(df_train_others)]
        else:
            temp_df = df_train_others[range_[i]:range_[i+1]]
        filename = "data/processed/external/others_train_" + str(i+1) + ".csv"
        temp_df.to_csv(filename, index = False)    
    print ("Done")

# filename = "data/processed/external/others_train_1.csv"
def replace_word_with_ner_others(filename):
    proc_name = current_process().name
    print ("%s started" %(proc_name))
    df_train = pd.read_csv(filename, usecols = [msg_column], encoding="utf-8")
    print ("[%s]: read df_train" %proc_name)
    splits = filename.split(".")
    ner_filename = splits[0] + "_ner." + splits[1] 
    file = open(ner_filename, "w+", encoding = "utf-8")
    count = 0
    for sentence in list(df_train[msg_column]):
        try:
            ner_tuple = nlp.ner(sentence)
            temp_sen = sentence
            count = count + 1
            for tuple_ in ner_tuple:
                if tuple_[1] != 'O':
                    temp_sen = temp_sen.replace(tuple_[0], tuple_[1])
        except Exception:
            temp_sen = sentence
            print("[%s] has error on line: %s and line number is %d" % (
                    proc_name, sentence, count))
        file.write(temp_sen + "\n")
        if count%100000 == 0:
            print ("[%s] count: %d" %(proc_name, count))
    file.close()


if __name__ == '__main__':
    try:
        pwd = 'C:\\Users\\ayush\\work\\event-types'
        os. chdir(pwd)
        files = ["data/processed/external/others_validate_1.csv", "data/processed/external/others_validate_2.csv", "data/processed/external/others_validate_3.csv"]

        procs = []
 
        for file_ in files:
            proc = Process(target=replace_word_with_ner_others, name = file_, args=(file_,))
            procs.append(proc)
            proc.start()
 
        for proc in procs:
            proc.join()

    except KeyboardInterrupt:
        print("Encountered keyboard error, closing all the processes")
        for proc in procs:
            proc.terminate()