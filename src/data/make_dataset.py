# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
from eventregistry import *
import json
import pandas as pd
import glob


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(input_filepath, output_filepath):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')


def download_er_data():
    __file__ = 'D:\\Users\\xlikedemo\\work\\event-type'
    project_dir = Path(__file__).resolve()

    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(filename=project_dir/'log' /
                        'download_er_data.log', level=logging.DEBUG, format=log_fmt)
    logger = logging.getLogger(__name__)

    load_dotenv(find_dotenv())
    er_api_key = os.environ.get("ER_API_KEY")
    if er_api_key == None:
        logger.error("Couldn't find the ER API Key")
    er = EventRegistry(apiKey=er_api_key)
    source_uri = get_source_uri(er)
    print (source_uri)
    q = QueryArticlesIter(
        dateStart="2014-01-01",
        dateEnd="2019-07-29",
        startSourceRankPercentile=0,
        endSourceRankPercentile=100,
        isDuplicateFilter="skipDuplicates",
        lang="eng",
        dataType= ['news', 'pr', 'blog'],
        sourceUri= source_uri
        )

    # obtain all the news articles for last 1 month sorted by date
    logger.info("Starting to download the ER data")
    number_of_articles = 0
    articles = []
    remaining_articles = True
    for art in q.execQuery(er, sortBy="date"):
        number_of_articles = number_of_articles + 1
        articles.append(art)
        if number_of_articles % 100 == 0:
            logger.info(
                "Total Number of articles downloaded so far: %d", number_of_articles)
            remaining_articles = False
            prefix = str(int(number_of_articles/100))
            filename = prefix + '_er_data.json'
            with open(project_dir/'data'/'external/peter'/filename, 'w') as f:
                f.write(json.dumps(articles, indent=4, ensure_ascii=True))
            f.close()
            articles = []

    if remaining_articles:
        prefix = str(int(number_of_articles/100) + 1)
        filename = prefix + '_er_data.json'
        with open(project_dir/'data'/'external/peter'/filename, 'w') as f:
            f.write(json.dumps(articles, indent=4, ensure_ascii=True))
        f.close()

    logger.info("Downloaded all ER data")

def get_source_uri(er):
    uri_list = []
    with open('data/business_news_sources.txt', 'r') as f:
        uris = f.readlines()
        for uri in uris:
            uri_list.append(er.getSourceUri(uri.strip("\n")))    
    return uri_list

def extract_news_body_from_json():
    for file in glob.glob("data/external/*_er_data.json"):
        df = pd.DataFrame()
        all_lines = []
        with open(file, 'r') as f:
            json_list = json.load(f)
            for json_ in json_list:
                news_body = json_["body"]
                # print (news_body)
                news_body = news_body.split("\n\n")
                for para in news_body:
                    lines = para.split(". ")
                    # print (lines)
                    for line in lines:
                        # print (line)
                        all_lines.append(line)
    df['EVENT_TEXT'] = all_lines
    suffix = file.split("/")[1].split("\\")[1].split("_")[0]
    filename = "data/interim/external/" + suffix + ".csv"
    df.to_csv(filename, index=False)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    __file__ = 'D:\\Users\\xlikedemo\\work\\event-type'
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    input_filepath = project_dir/'data'/'raw'
    output_filepath = project_dir/'data'/'processed'
    main(input_filepath, output_filepath)
