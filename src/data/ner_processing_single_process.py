import glob
import os
from multiprocessing import Process, current_process
import sys
import pandas as pd
from stanfordcorenlp import StanfordCoreNLP
import logging


class KeyboardInterruptError(Exception):
    pass


# Important columns in the data
msg_column = 'EVENT_TEXT'
msg_clean_column = 'EVENT_TEXT_CLEANED'
msg_column_ner = 'EVENT_TEXT_NER'
topic_column = 'TOPIC'
group_column = 'GROUP'
type_column = 'TYPE'
topic_group_column = topic_column + group_column
topic_group_type_column = topic_column + group_column + type_column
file_name = sys.argv[0]
pwd = 'C:\\Users\\ayush\\work\\event-type'
log_filename = pwd + '/log/' + file_name.split(".")[0].split("\\")[-1] + '.log'
log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(filename=log_filename,
                    level=logging.INFO, format=log_fmt)
logger = logging.getLogger()
nlp = StanfordCoreNLP('http://localhost', port=9000)
logger.info("Connected to NLP server")


def smaller_files():
    df_train_others = pd.read_csv("data/processed/all/train.csv")
    range_ = range(0, len(df_train_others), 2000000)
    for i in range(0, len(range_)):
        print(i+1)
        if i == len(range_) - 1:
            temp_df = df_train_others[range_[i]:len(df_train_others)]
        else:
            temp_df = df_train_others[range_[i]:range_[i+1]]
        filename = "data/processed/all/train_" + str(i+1) + ".csv"
        temp_df.to_csv(filename, index=False)
    print("Done")

# filename = "data/processed/all/train_1_1.csv"


def replace_word_with_ner(filename):
    proc_name = current_process().name
    count = 0
    logger.info("%s started" % (proc_name))
    df_train = pd.read_csv(filename, usecols=[msg_column], encoding="utf-8")
    splits = filename.split(".")
    ner_filename = splits[0] + "_ner_new." + splits[1]
    logger.info("[%s]: NER filename is: %s" % (proc_name, ner_filename))
    try:
        try:
            file_ = open(ner_filename, "w+", encoding="utf-8")
        except Exception:
            logger.exception("[%s]:Unable to open %s" %
                             (proc_name, ner_filename))
            sys.exit(1)

        for sentence in list(df_train[msg_column]):
            try:
                ner_tuple = nlp.ner(sentence)
                temp_sen = sentence
                for tuple_ in ner_tuple:
                    if tuple_[1] != 'O':
                        temp_sen = temp_sen.replace(tuple_[0], tuple_[1])
            except Exception:
                temp_sen = sentence
                logger.exception("[%s] has error on line: %s and line number is %d" % (
                    proc_name, sentence, count))
            try:
                file_.write(sentence + "\t\t" + temp_sen + "\n")
            except Exception:
                logger.exception("[%s]: Unable to write to file %s" %
                                 (proc_name, ner_filename))

            count += 1
            if count % 100000 == 0:
                logger.info("[%s] count: %d" % (proc_name, count))
        file_.close()
        logger.info("[%s]: Completed processing %s" % (proc_name, filename))
    except KeyboardInterrupt:
        logger.info("[%s]: Number of lines processed is %d" %
                    (proc_name, count))
        count_file = open(ner_filename.split(
            ".")[0] + "_count.txt", "w+", encoding="utf-8")
        count_file.write(str(count))
        count_file.close()
        raise KeyboardInterruptError


def restart_replace_word_with_ner(filename):
    proc_name = current_process().name
    print("%s started" % (proc_name))
    splits = filename.split(".")
    ner_filename = splits[0].split(
        "\\")[0] + "/ner_files/" + splits[0].split("\\")[1] + "_ner." + splits[1]
    print("[%s] ner filename is: %s" % (proc_name, ner_filename))
    file_ = open(ner_filename, "r", encoding="utf-8")
    lines = file_.readlines()
    count = len(lines)
    del lines
    file_.close()
    print("Number of lines already processed for file %s is : %d" %
          (ner_filename, count))
    df_train = pd.read_csv(filename, skiprows=count, encoding="utf-8")
    print(df_train.head(2))
    file_ = open(ner_filename, "a", encoding="utf-8")
    for sentence in list(df_train[msg_column]):
        try:
            ner_tuple = nlp.ner(sentence)
            temp_sen = sentence
            for tuple_ in ner_tuple:
                if tuple_[1] != 'O':
                    temp_sen = temp_sen.replace(tuple_[0], tuple_[1])
        except Exception as ex:
            temp_sen = sentence
            print("[%s] has error on line: %s" % (proc_name, sentence))
            print(ex)
        count = count + 1
        file_.write(temp_sen + "\n")
        if count % 100000 == 0:
            print("[%s] count: %d" % (proc_name, count))
    file_.close()


if __name__ == '__main__':
    try:
        os.chdir(pwd)
        file_ = "data/processed/all/train_16_1.csv"
        replace_word_with_ner(file_)        
    except KeyboardInterrupt:
        logger.error("Encountered keyboard error, closing all the processes")
    
