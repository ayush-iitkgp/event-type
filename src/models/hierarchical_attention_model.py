from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras import backend as K, initializers, regularizers, constraints
from keras.engine.topology import Layer
from keras.layers import Input
from keras.layers.core import Dropout, Dense, Lambda, Masking
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import GRU, LSTM
from keras.layers.wrappers import Bidirectional, TimeDistributed
from keras.models import Model

# Copied from https://gist.github.com/rmdort/596e75e864295365798836d9e8636033


class AttentionWithContext(Layer):
    """
        Attention operation, with a context/query vector, for temporal data.
        Supports Masking.
        Follows the work of Yang et al. [https://www.cs.cmu.edu/~diyiy/docs/naacl16.pdf]
        "Hierarchical Attention Networks for Document Classification"
        by using a context vector to assist the attention
        # Input shape
            3D tensor with shape: `(samples, steps, features)`.
        # Output shape
            2D tensor with shape: `(samples, features)`.
        :param kwargs:
        Just put it on top of an RNN Layer (GRU/LSTM/SimpleRNN) with return_sequences=True.
        The dimensions are inferred based on the output shape of the RNN.
        Example:
            model.add(LSTM(64, return_sequences=True))
            model.add(AttentionWithContext())
        """

    def __init__(self, init='glorot_uniform', kernel_regularizer=None, bias_regularizer=None, kernel_constraint=None, bias_constraint=None,  **kwargs):
        self.supports_masking = True
        self.init = initializers.get(init)
        self.kernel_initializer = initializers.get('glorot_uniform')

        self.kernel_regularizer = regularizers.get(kernel_regularizer)
        self.bias_regularizer = regularizers.get(bias_regularizer)

        self.kernel_constraint = constraints.get(kernel_constraint)
        self.bias_constraint = constraints.get(bias_constraint)

        super(AttentionWithContext, self).__init__(**kwargs)

    def build(self, input_shape):
        self.kernel = self.add_weight((input_shape[-1], 1),
                                      initializer=self.kernel_initializer,
                                      name='{}_W'.format(self.name),
                                      regularizer=self.kernel_regularizer,
                                      constraint=self.kernel_constraint)
        self.b = self.add_weight((input_shape[1],),
                                 initializer='zero',
                                 name='{}_b'.format(self.name),
                                 regularizer=self.bias_regularizer,
                                 constraint=self.bias_constraint)

        self.u = self.add_weight((input_shape[1],),
                                 initializer=self.kernel_initializer,
                                 name='{}_u'.format(self.name),
                                 regularizer=self.kernel_regularizer,
                                 constraint=self.kernel_constraint)
        self.built = True

    def compute_mask(self, input, mask):
        return None

    def call(self, x, mask=None):
        # (x, 40, 300) x (300, 1)
        multData = K.dot(x, self.kernel)  # (x, 40, 1)
        multData = K.squeeze(multData, -1)  # (x, 40)
        multData = multData + self.b  # (x, 40) + (40,)

        multData = K.tanh(multData)  # (x, 40)

        multData = multData * self.u  # (x, 40) * (40, 1) => (x, 1)
        multData = K.exp(multData)  # (X, 1)

        # apply mask after the exp. will be re-normalized next
        if mask is not None:
            mask = K.cast(mask, K.floatx())  # (x, 40)
            multData = mask*multData  # (x, 40) * (x, 40, )

        # in some cases especially in the early stages of training the sum may be almost zero
        # and this results in NaN's. A workaround is to add a very small positive number ε to the sum.
        # a /= K.cast(K.sum(a, axis=1, keepdims=True), K.floatx())
        multData /= K.cast(K.sum(multData, axis=1,
                                 keepdims=True) + K.epsilon(), K.floatx())
        multData = K.expand_dims(multData)
        weighted_input = x * multData
        return K.sum(weighted_input, axis=1)

    def compute_output_shape(self, input_shape):
        return (input_shape[0], input_shape[-1],)


def create_hierarchical_attention_model(max_senten_len, max_senten_num, nb_classes, activation, metrics, loss,
                                        emb_weights, embedding_size=None,  optimizer='adam',
                                        recursive_class=LSTM, word_rnn_size=100, sentenceRnnSize=100,
                                        drop_word_emb=0.5, drop_word_rnn_out=0.5, drop_sentence_rnn_out=0.5):
    # Words level attention model
    word_input = Input(shape=(max_senten_len,), dtype='float32')

    word_sequences = Embedding(
        emb_weights.shape[0], emb_weights.shape[1], mask_zero=True, weights=[emb_weights], trainable = True)(word_input)

    word_lstm = Bidirectional(LSTM(150, return_sequences=True))(word_sequences)
    word_dense = TimeDistributed(Dense(200))(word_lstm)
    word_att = AttentionWithContext()(word_dense)
    wordEncoder = Model(word_input, word_att)
    wordEncoder.layers[1].trainable = True

    # Sentence level attention model
    sent_input = Input(shape=(max_senten_num, max_senten_len), dtype='float32')
    sent_encoder = TimeDistributed(wordEncoder)(sent_input)
    sent_lstm = Bidirectional(LSTM(150, return_sequences=True))(sent_encoder)
#     sent_dense = TimeDistributed(Dense(200))(sent_lstm)
    sent_att = Dropout(0.5)(AttentionWithContext()(sent_lstm))
    preds = Dense(nb_classes, activation=activation)(sent_att)
    model = Model(sent_input, preds)
    model.compile(loss=loss,
                  optimizer=optimizer, metrics=metrics)

    return model


def create_hierarchical_attention_model_without_embedding_layer(max_senten_len, max_senten_num, nb_classes, activation, metrics, loss, embedding_len, embedding_size=None,  optimizer='adam',
                                        recursive_class=LSTM, word_rnn_size=100, sentenceRnnSize=100,
                                        drop_word_emb=0.5, drop_word_rnn_out=0.5, drop_sentence_rnn_out=0.5):
    # Words level attention model
    word_input = Input(shape=(max_senten_len, embedding_len), dtype='float32')

    # word_sequences = Embedding(
    #     emb_weights.shape[0], emb_weights.shape[1], mask_zero=True, weights=[emb_weights], trainable = True)(word_input)

    word_lstm = Bidirectional(LSTM(150, return_sequences=True))(word_input)
    word_dense = TimeDistributed(Dense(200))(word_lstm)
    word_att = AttentionWithContext()(word_dense)
    wordEncoder = Model(word_input, word_att)

    # Sentence level attention model
    sent_input = Input(shape=(max_senten_num, max_senten_len, embedding_len), dtype='float32')
    sent_encoder = TimeDistributed(wordEncoder)(sent_input)
    sent_lstm = Bidirectional(LSTM(150, return_sequences=True))(sent_encoder)
#     sent_dense = TimeDistributed(Dense(200))(sent_lstm)
    sent_att = Dropout(0.5)(AttentionWithContext()(sent_lstm))
    preds = Dense(nb_classes, activation=activation)(sent_att)
    model = Model(sent_input, preds)
    model.compile(loss=loss,
                  optimizer=optimizer, metrics=metrics)

    return model
