import logging
import os
import keras
from keras import backend as K
import pandas as pd
from src.embeddings.preproc import line
from fastText import load_model
import numpy as np
from keras.metrics import categorical_accuracy
from keras.losses import categorical_crossentropy
from src.models import hierarchical_attention_model
from sklearn.preprocessing import LabelBinarizer
import datetime
import sys
from keras.callbacks import EarlyStopping, ModelCheckpoint
import time

max_senten_num = 1  # Every document has only 1 sentence_
MAX_SEQUENCE_LENGTH = 13
# Important columns in the data
msg_column = 'EVENT_TEXT'
msg_clean_column = 'EVENT_TEXT_CLEANED'
topic_column = 'TOPIC'
group_column = 'GROUP'
type_column = 'TYPE'
msg_clean_fasttext_column = 'EVENT_TEXT_FASTTEXT_CLEANED'
topic_group_column = topic_column + group_column
topic_group_type_column = topic_column + group_column + type_column
pwd = 'C:\\Users\\ayush\\work\\event-types'
os. chdir(pwd)
count = 0
file_name = sys.argv[0]
print (file_name)
log_filename = pwd + '/log/' + file_name.split(".")[0].split("\\")[-1] + '.log'
log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(filename=log_filename,
                        level=logging.DEBUG, format=log_fmt)
logger = logging.getLogger(__name__)

ft_model = load_model(
        'data\\embeddings\\fasttext\\downloaded\\crawl-300d-2M-subword\\crawl-300d-2M-subword.bin')
n_features = ft_model.get_dimension()


def data_preprocessing(sentence):
    global count
    count = count + 1
    if count % 1000000 == 0:
        logger.info("Number of lines processed: %d" % (count))
    return line(sentence)


def text_to_vector(text):
    """
    Given a string, normalizes it, then splits it into words and finally converts
    it to a sequence of word vectors.
    """
    words = text.split()
    window = words[0:MAX_SEQUENCE_LENGTH]

    x = np.zeros((MAX_SEQUENCE_LENGTH, n_features))

    for i, word in enumerate(window):
        x[i, :] = ft_model.get_word_vector(word).astype('float32')

    return x


def df_to_data(df):
    """
    Convert a given dataframe to a dataset of inputs for the NN.
    """
    n_features = 300
    x = np.zeros((len(df), MAX_SEQUENCE_LENGTH, n_features), dtype='float32')

    for i, comment in enumerate(df[msg_clean_fasttext_column].values):
        x[i, :] = text_to_vector(comment)
        if i % 1000000 == 0:
            logger.info("Size of input matrix is: %d" %(i))
    return x


def prepare_y_train():
    topic_group_type_labels = list(
        df_train[topic_group_type_column]) + list(df_others_train[topic_group_type_column])
    logger.info("Len of y_train is %d" % (len(topic_group_type_labels)))

    encoder=LabelBinarizer(sparse_output=True)
    y_train=encoder.fit_transform(topic_group_type_labels)
    logger.info("Number of classes in y_train is: %d" %
                (len(encoder.classes_)))
    # logger.info(y_train[0])
    logger.info("Shape of y_train is: (%d, %d)" % (y_train.shape[0], y_train.shape[1]))
    return encoder, y_train


if __name__ == '__main__':
    # Check if the GPU is being used or not
    logger.info(keras.backend.backend())
    logger.info(K.tensorflow_backend._get_available_gpus())
    logger.info(keras.__version__)

    # Read the train, validate and test set for all classes except "Other class"
    df_train=pd.read_csv("data/processed/sample/sample_train.csv")
    # Read the train, validate and test set for all classes except "Other class"
    df_others_train=pd.read_csv(
        "data/processed/sample/sample_others_train.csv")

    logger.info("Read df_train and df_others_train")

    nb_classes=df_train[topic_group_type_column].nunique(
    ) + df_others_train[topic_group_type_column].nunique()
    activation='softmax'
    metrics=['categorical_accuracy']
    loss='categorical_crossentropy'
    
    model=hierarchical_attention_model.create_hierarchical_attention_model_without_embedding_layer(MAX_SEQUENCE_LENGTH,
                                                                                                     max_senten_num, nb_classes, activation, metrics, loss, n_features)
    logger.info("Model summary is %s" % (model.summary()))
    encoder, y_train = prepare_y_train()

    df = pd.concat([df_train, df_others_train], ignore_index=False)

    del df_train
    del df_others_train

    logger.info("Deleted df_train and df_others_train from memory")

    df[msg_clean_fasttext_column]=df[msg_column].apply(
        lambda x: data_preprocessing(x))

    logger.info("Data preprocessing completed")

    x_train = df_to_data(df)
    logger.info("Length of x_train is (%d, %d)" % (x_train.shape[0], x_train.shape[1]))

    del df
    logger.info("Removed df from the memory")

    # Prepare y_val
    df_others_validate=pd.read_csv(
        "data/processed/sample_others_validate.csv")
    df_validate=pd.read_csv("data/processed/sample_validate.csv")

    def prepare_y_val():
        return encoder.transform(list(df_validate[topic_group_type_column]) + list(df_others_validate[topic_group_type_column]))

    y_val=prepare_y_val()
    logger.info("Shape of y_val is (%d, %d)" %(y_val.shape[0], y_val.shape[1]))

    x_val=df_to_data(
        pd.concat([df_validate, df_others_validate], ignore_index=False))
    logger.info("Shape of x_val is (%d, %d)" %(x_val.shape[0], x_val.shape[1]))

    del df_validate
    del df_others_validate
    logger.info("Removed df_validate and df_others_validate from the memory")

    current_date=datetime.datetime.now().strftime("%Y-%m-%d")

    saved_model_location=os.getcwd() + '\\models\\' + \
        file_name.split(".")[0].split("\\")[-1] + '\\' + current_date + '\\'

    # Train the DL model
    # Early Stopping
    early_stopping=EarlyStopping(patience=1, verbose=1)

    checkpointer=ModelCheckpoint(
        filepath=saved_model_location + "-" + "{epoch:02d}-" + "{val_loss:.2f}" + str(
            time.time()) + ".hdf5",
        verbose=1, save_best_only=False)

    logger.info("Starting to train the model")
    history=model.fit(x_train, y_train, validation_data=(x_val, y_val), nb_epoch=10, batch_size=1000,
                        verbose=1, callbacks=[early_stopping, checkpointer])
    
